import msvcrt #MODULO PARA QUE EL PROGRAMA NO SE CIERRE DURANTE LA EJECUCION
class Nodo:

  def __init__ (self,estado,padre): 
    self.estado = estado
    self.padre = padre


  def Recorrido_Camino(self):
        lista_nuevos_estados = []
        contador = 1
        nodo_actual = self

        while nodo_actual.padre is not None:
            lista_nuevos_estados.append(nodo_actual.estado)
            nodo_actual = nodo_actual.padre
        lista_nuevos_estados.reverse()
           
        
        for estado in lista_nuevos_estados:
            print("Estado #: ",contador)
            print('-------')
            mostrar_estado(estado)
            print('-------')
            print('\n')
            contador += 1

        return lista_nuevos_estados


def mov_Arriba(estado):
  nuevo_estado = estado [:]
  posicion = nuevo_estado.index(0)
    
  if posicion not in [0, 1, 2]:
    V_temp = nuevo_estado [posicion - 3]
    nuevo_estado[posicion - 3] = nuevo_estado[posicion]
    nuevo_estado[posicion] = V_temp
    return (nuevo_estado)
  else:
    return None

def mov_Derecha(estado):
  nuevo_estado = estado [:]
  posicion = nuevo_estado.index(0)
    
  if posicion not in [2, 5, 8]:
    V_temp = nuevo_estado [posicion + 1]
    nuevo_estado[posicion + 1] = nuevo_estado[posicion]
    nuevo_estado[posicion] = V_temp
    return (nuevo_estado)
  else:
    return None

def mov_Abajo(estado):
  nuevo_estado = estado [:]
  posicion = nuevo_estado.index(0)
    
  if posicion not in [6, 7, 8]:
    V_temp = nuevo_estado [posicion - 3]
    nuevo_estado[posicion - 3] = nuevo_estado[posicion]
    nuevo_estado[posicion] = V_temp
    return (nuevo_estado)
  else:
    return None


def mov_Izquierda(estado):
  nuevo_estado = estado [:]
  posicion = nuevo_estado.index(0)
    
  if posicion not in [0, 3, 6]:
    V_temp = nuevo_estado [posicion - 1]
    nuevo_estado[posicion - 1] = nuevo_estado[posicion]
    nuevo_estado[posicion] = V_temp
    return (nuevo_estado)
  else:
    return None


def mostrar_estado(estado):
    renglon = 0
    for n in estado:
        print(n, end=" ")
        renglon += 1
        if renglon == 3:
            print()
            renglon = 0


def expansion_nodos(nodo):
    nodos_expandidos = []

    nodos_expandidos.append(Nodo(mov_Arriba(nodo.estado), nodo))
    nodos_expandidos.append(Nodo(mov_Abajo(nodo.estado), nodo))
    nodos_expandidos.append(Nodo(mov_Derecha(nodo.estado), nodo))
    nodos_expandidos.append(Nodo(mov_Izquierda(nodo.estado), nodo))

    nodos_expandidos = [nodo for nodo in nodos_expandidos if nodo.estado != None]
    return nodos_expandidos



def main():
    estado_objetivo = [1, 2, 3, 4, 5, 6, 7, 8, 0]
    estado_inicial = [8, 7, 5, 3, 0, 1, 4, 2, 6]

    print("Este programa encuentra la solución al 8-puzzle")
    print("\t\"bfs\" para  correr Busqueda en anchura")
    print("\t\"dfs\" para  correr Busqueda en profundidad")
    algoritmo = input("Su elección: ")
    if algoritmo == "bfs" or algoritmo == "BFS":
        print("Corriendo BFS. Por favor espere.")
        resultado = BFS(estado_inicial, estado_objetivo)
        print('La solucion se encontro en:', len(resultado),'pasos')

    elif algoritmo == "dfs" or algoritmo == "DFS":
        print("Corriendo DFS. Por favor espere.")
        resultado = DFS(estado_inicial, estado_objetivo)
        print('La solucion se encontro en:', len(resultado),'pasos')


    else:
        return 0



def BFS(inicial, meta):
    nodos = []
    nodos.append(Nodo(inicial, None))

    nodos_descubiertos = set()

    while nodos:
        nodo_actual = nodos.pop(0)

        if tuple(nodo_actual.estado) not in nodos_descubiertos:
            nodos_descubiertos.add(tuple(nodo_actual.estado))
        else:
            continue

        if nodo_actual.estado == meta:
            return nodo_actual.Recorrido_Camino()
        else:
            nodos.extend(expansion_nodos(nodo_actual))


def DFS(inicial, meta):
    nodos = []
    nodos.append(Nodo(inicial, None))

    nodos_descubiertos = set()

    while nodos:
        nodo_actual = nodos.pop()

        if tuple(nodo_actual.estado) not in nodos_descubiertos:
            nodos_descubiertos.add(tuple(nodo_actual.estado))
        else:
            continue

        if nodo_actual.estado == meta:
            return nodo_actual.Recorrido_Camino()
        else:
            nodos.extend(expansion_nodos(nodo_actual))






if __name__ == "__main__":
    main()

msvcrt.getch()







